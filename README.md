## 周浦微电子社区小程序

### 注意事项

- 每次提交代码前，在个人中心页，`/pages/my/my`的`<view class="version-container"><text>V2021.11.19.13.21</text></view>`这句代码中修改版本号，版本号规则就是年月日时分。
- 上传代码到微信平台，直接运行`node uploadCode.js`不需要手动在IDE上点击`上传`按钮儿。