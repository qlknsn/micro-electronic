const ci = require("miniprogram-ci");
let as = async () => {
  const project = new ci.Project({
    appid: "wxd5dd4b7d418c2122",
    type: "miniProgram",
    projectPath: "./",
    privateKeyPath: "./private.wxd5dd4b7d418c2122.key",
    ignores: ["node_modules/**/*"],
  });
  let d = new Date()
  let desc = process.argv.splice(2)[0]
  const uploadResult = await ci.upload({
    project,
    version: d.getFullYear() + "." + (d.getMonth() + 1) + "." + d.getDate() + "." + d.getHours() + "." + d.getMinutes() + "." + d.getSeconds(),
    desc: desc,
    setting: {
      es6: true,
    },
    onProgressUpdate: console.log,
  });
  console.log(uploadResult);
};
as();