import { imgUrl } from "../../request/publicData"

// pages/repairs/repairs.js
import {getRepairList, deleteRepair, getReportList, deleteReport} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: imgUrl,
    cometype: '',
    inputShowed: false,
    inputVal: "",
    ismore: false,
    tabs: [],
    activeTab: 0,
    dialogShow: false,
    buttons: [{text: '取消'}, {text: '确定'}],
    start: 0 ,
    content: '',
    uniqueId: '',
    type: 0,
    isnothing: false,
    pageType: '3',
    userInfo: {},
    repairList: []
  },
  selectResult: function (e) {
  },
  onTabCLick(e) {
    const index = e.detail.index
    this.setData({activeTab: index})
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.setData({
      userInfo: userInfo,
      pageType: options.type
    })
    if (options.type === '3') {
      this.getRepairList()
    } else {
      this.getReportList()
    }
    this.setData({
      search: this.search.bind(this)
    })
    const titles = ['待处理', '已处理']
    const tabs = titles.map(item => ({title: item}))
    this.setData({tabs})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.ismore) {
      this.getRepairList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  todetail(e){
    if(this.data.pageType === '3') {
      wx.navigateTo({
        url: '/pages/repairDetail/repairDetail?id=' + e.currentTarget.dataset.uniqueid,
      })
    } else {
      wx.navigateTo({
        url: '/pages/reportDetail/reportDetail?id=' + e.currentTarget.dataset.uniqueid,
      })
    }
  },
  deleterepaire (e) {
    this.setData({
      dialogShow: true,
      uniqueId: e.currentTarget.dataset.uniqueid
    })
  },
  tapDialogButton(e) {
    this.setData({
        dialogShow: false,
        showOneButtonDialog: false
    })
    if (e.detail.index === 0) {
    } else {
      let params = {
        uniqueId: this.data.uniqueId
      }
      this.setData({
        start: 0,
        repairList: []
      })
      if (this.data.pageType === '3') {
        deleteRepair(params).then(res => {
          this.getRepairList()
        })            
      } else {
        deleteReport(params).then(res => {
          this.getReportList()
        })
      }
    }
  },
  clearinput() {
    this.setData({
      start: 0,
      repairList: []
    })
    if(this.data.pageType === '3') {
      this.getRepairList('')
    } else {
      this.getReportList('')
    }
  },
  onChange(e) {
    let type = ''
    if(e.detail.index === 0) {
      type = 0
    } else if(e.detail.index === 1){
      type = 1
    }
    this.setData({
      type: type,
      repairList: [],
      start: 0
    })
    if (this.data.pageType === '3') {
      this.getRepairList()
    } else {
      this.getReportList()
    }   
  },
  getRepairList: function(content = '') {
    const params = {
      count: 10,
      start: this.data.start,
      mobile: this.data.userInfo.mobile,
      districtIds: this.data.userInfo.districtId,
      type: this.data.type, // 0是待处理，1是已处理
    }
    if (content) {
      params.content = content
    }
    getRepairList(params).then(res => {
      let {pagination, results} = res.data
      if(results.length === 0 && pagination.start === 0) {
        this.setData({
          isnothing: true
        })
      } else {
        results.forEach(item => {
          if (item.createTime) {
            item.createTimewen  = item.createTime.substring(0,10)
          }
        })
        let ismore = false
        let start = this.data.start
        if(pagination.total > pagination.start){
          ismore = true
          start = this.data.start + 10
        } else {
          ismore = false
        }
        this.setData({
          repairList: [...this.data.repairList, ...res.data.results],
          ismore: ismore,
          start: start,
          isnothing: false
        })
      }
    })
  },
  getReportList(content = '') {
    const params = {
      count: 10,
      districtIds: this.data.userInfo.districtId,
      mobile: this.data.userInfo.mobile,
      releaseStatus: '1',
      start: this.data.start,
      type: this.data.type,
      userUrn: this.data.userInfo.userUrn
    }
    if (content) {
      params.content = content
    }
    getReportList(params).then(res => {
      let {pagination, results} = res.data
      results.forEach(item => {
        if (item.createTime) {
          item.createTimewen  = item.createTime.substring(0,10)
          if (item.repairTime) {
            item.repairTime  = item.repairTime.substring(0,10)
          }
        }
      })
      this.setData({
        repairList: results,
        pagination: pagination
      })
      if (results.length === 0) {
        this.setData({
          isnothing: true
        })
      } else {
        this.setData({
          isnothing: false
        })
      }
    })
  },
  search: function (value) {
    return new Promise((resolve, reject) => {
      this.setData({
        start: 0,
        repairList: [],
        content: value
      })
      if(this.data.pageType === '3') {
        this.getRepairList(value)
      } else {
        this.getReportList(value)
      }
    })
  },
})