// pages/todoTask/todoTask.js
// import Request from "../../service/http";
// import GetTrackingInfo from "../../service/getTrackingrouterInfo";
import {
  getGuaranteeH
} from '../../request/getIndexData'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    taskList: [],
    taskId: "",
    taskName: '',
    isPickerRender: false,
    isPickerShow: false,
    startTime: "",
    endTime: "",
    pageType: "0", // 0的时候是待接单，1的时候处置中
    page: 1,
    totalPage: 0,
    nomore: false,
    pickerConfig: {
      endDate: true,
      column: "second",
      dateLimit: true,
      initStartTime: "2020-01-01 00:00:00",
      initEndTime: "2020-12-01 00:00:00",
      limitStartTime: "2015-05-06 00:00:00",
      limitEndTime: "2055-05-06 00:00:00",
    },
    statusList: [],
    status: 0,
    index: 0,
  },
  bindPickerChange(e) {
    let int = 0
    for (let i = 0; i < this.data.statusList.length; i++) {
      if (e.detail.value == i) {
        int = this.data.statusList[i].code
      }
    }
    this.setData({
      index: e.detail.value,
      status: int,
      taskList: []
    })
    this.getTaskLists()
  },
  getTaskLists: function () {
    let params = {
      "start": 0,
      "count": 1000,
      "type": this.data.status
    }
    if (this.data.taskName) {
      params.content = this.data.taskName
    }
    if (this.data.startTime) {
      params.startTime = this.data.startTime
    }
    if (this.data.endTime) {
      params.endTime = this.data.endTime
    }
    getGuaranteeH(params).then(res => {
      this.setData({
        taskList: res.data.results
      })
    }).catch(err => console.log(err))
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      statusList: [{
          code: 0,
          value: "处置中",
        },
        {
          code: 1,
          value: "已完成",
        }
      ]
    })
    this.setData({})
    this.getTabBar().setData({
      list: [{
          "pagePath": "/pages/historyFinish/historyFinish",
          "iconPath": "../image/home.png",
          "selectedIconPath": "../image/shouye@2x.png",
          "text": "首页"
        },
        {
          "pagePath": "/pages/my/my",
          "iconPath": "../image/wode1@2x.png",
          "selectedIconPath": "../image/wode@2x.png",
          "text": "我的"
        }
      ]
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let status = wx.getStorageSync('wuyeOrderStatus')
    console.log(status)
    if (status == 'finish') {
      this.setData({
        index: 1
      });
      this.setData({
        status: 1
      })
    } else {
      this.setData({
        index: 0
      });
      this.setData({
        status: 0
      })
    }
    this.getTabBar().setData({
      selected: 0
    });
    this.getTaskLists()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
  pickerShow: function () {
    this.setData({
      isPickerShow: true,
      isPickerRender: true,
      chartHide: true,
    });
  },
  pickerHide: function () {
    this.setData({
      isPickerShow: false,
      chartHide: false,
    });
  },
  setPickerTime: function (val) {
    let data = val.detail;
    this.setData({
      startTime: data.startTime,
      endTime: data.endTime,
      dataList: [],
    });
    this.getTaskLists();
  },
  gotoTaskDetail: function (e) {
    // this.setData({
    //   taskId: e.currentTarget.dataset.id,
    // });
    if (e.currentTarget.dataset.item.type == '1') {
      wx.navigateTo({
        url: `/pages/taskDetail/taskDetail?uniqueId=${e.currentTarget.dataset.item.uniqueId}`,
      })
    } else {
      wx.navigateTo({
        url: `/pages/taskHandle/taskHandle?uniqueId=${e.currentTarget.dataset.item.uniqueId}`,
      });
    }

  },
  getTodoList: function () {
    const params = {
      type: this.data.pageType, // 1=接单大厅 2=待办任务 3=历史任务 4=随手报
      limit: 10,
      page: this.data.page,
      taskName: this.data.taskName,
      startTime: this.data.startTime,
      endTime: this.data.endTime,
    };
    let a = new Request("/url/task/v4/task/list", params);
    a.post().then((res) => {
      let {
        data: {
          data: {
            list,
            totalPage
          },
          code,
        },
      } = res;
      if (code == 200) {
        let arr = [...this.data.dataList, ...list];
        this.setData({
          dataList: arr,
          totalPage: totalPage,
        });
      }
    });
  },
  bindKeyInput: function (e) {
    this.setData({
      taskName: e.detail.value,
      dataList: [],
    });
    this.getTaskLists();
  },
});