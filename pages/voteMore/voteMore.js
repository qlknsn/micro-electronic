// pages/vote/vote.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [
      {value: 'USA', name: '美国', checked: false},
      {value: 'CHN', name: '中国', checked: false},
      {value: 'BRA', name: '巴西', checked: false},
      {value: 'JPN', name: '日本', checked: true},
      {value: 'ENG', name: '英国', checked: false},
      {value: 'FRA', name: '法国', checked: false},
    ],
    headerBoxRightGray:'#80C22B'
  },

  moreVote(e){
    let currentValue = e.currentTarget.dataset.value;
    let tempItems = this.data.items;
    for(let i = 0;i < tempItems.length;i++){
      if(tempItems[i].value == currentValue){
        tempItems[i].checked = !tempItems[i].checked
      }
    }
    this.setData({
      items:tempItems
    });
    console.log(this.data.items)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})