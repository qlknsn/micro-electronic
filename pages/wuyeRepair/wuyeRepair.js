// pages/wuyeRepair/wuyeRepair.js
import {
  saveGuarantee
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    descriptionText: '',
    textLength: 0,
    files: [],
    userReceivedImgList: [],
    params: {
      filePaths: [],
      guaranteeObject: {
        content: '',
        districtId: '',
        districtName: '',
        mobile: '',
        name: '',
        neighbourhood: '',
        lat: '',
        lng: ''
      }
    },
    areaname: ''
  },
  getloacation() {
    let that = this
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userLocation']) {
          wx.getLocation({
            type: 'gcj02',
            success(r) {
              that.data.params.guaranteeObject.lat = r.latitude
              that.data.params.guaranteeObject.lng = r.longitude
            }
          })
        } else {
          wx.openSetting({
            success(res) {
              // 2.1 如果二次授权允许了 userLocation 权限， 就再次执行获取位置的接口
              if (res.authSetting["scope.userLocation"]) {
                wx.getLocation({
                  success(r) {
                    // 2.2 获取用户位置成功后，将会返回 latitude, longitude 两个字段，代表用户的经纬度位置
                    // 2.3 将获取到的 经纬度传值给 getAddress 解析出 具体的地址
                    that.data.params.guaranteeObject.lat = r.latitude
                    that.data.params.guaranteeObject.lng = r.longitude
                  }
                })
              }
            }
          })
        }
      }
    })
  },
  bindTextAreaBlur(e) {
    // console.log(e.detail.value.length)
    if (e.detail.value.length == 501) {
      wx.showToast({
        title: '最多只能输入500字',
      })
    } else {
      this.setData({
        textLength: e.detail.value.length
      })
      this.data.params.guaranteeObject.content = e.detail.value
    }

  },
  handleDeleteImg: function (params) {
    let d = params.currentTarget.dataset.index
    let list = this.data.files
    let receiveList = this.data.userReceivedImgList
    list.splice(d, 1)
    receiveList.splice(d, 1)
    this.setData({
      files: list
    })
    this.setData({
      userReceivedImgList: receiveList
    })
  },
  chooseImage() {
    var that = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        if (res.tempFilePaths.length + that.data.files.length > 3) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传三张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths
          let temp = that.data.files
          tempFilePaths.forEach(item => {
            temp.push(item)
            that.setData({
              files: [...temp]
            })
            wx.uploadFile({
              filePath: item,
              name: 'file',
              url: `https://micro-electronic.bearhunting.cn/public/api/hip/v1/attachment/fileUpload`,
              header: {
                "Content-Type": "multipart/form-data",
                "token": wx.getStorageSync('token')
              },
              success: function (res) {
                if (res.statusCode == 200) {
                  let d = JSON.parse(res.data).result
                  let temp = that.data.userReceivedImgList
                  temp.push(d[0])
                  that.setData({
                    userReceivedImgList: temp
                  })
                } else {
                  wx.showModal({
                    title: '警告',
                    content: "上传失败，请重试",
                    showCancel: false
                  })
                  let temp = that.data.files
                  temp.pop()
                  that.setData({
                    files: [...temp]
                  })
                }
              }
            })
          })

        }

        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        // that.setData({
        //   files: that.data.files.concat(res.tempFilePaths)
        // });
      }
    })
  },
  submitsaveGuarantee() {
    if(!this.data.params.guaranteeObject.lat){
      this.getloacation()
      return false
    }

    this.data.params.filePaths = this.data.userReceivedImgList
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.guaranteeObject.mobile = userInfo.mobile
    this.data.params.guaranteeObject.name = userInfo.nickname
    this.data.params.guaranteeObject.address = userInfo.address
    this.data.params.guaranteeObject.portraitImg = userInfo.portraitImg
    this.data.params.guaranteeObject.districtId = userInfo.districtId
    this.data.params.guaranteeObject.districtName = userInfo.districtName
    this.data.params.guaranteeObject.neighbourhood = userInfo.districtName
    saveGuarantee(this.data.params).then(res => {
      if (res.data.error) {
        wx.showToast({
          title: '添加报修失败请重试',
        })
      } else {
        wx.showToast({
          title: '添加报修成功请去列表页查看',
        })
        wx.reLaunch({
          url: '/pages/index/index',
        })
        // this.getTabBar().setData({
        //   selected: 0
        // });
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.setData({
      areaname: userInfo.districtName
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let that = this
    wx.getLocation({
      success(r) {
        // 2.2 获取用户位置成功后，将会返回 latitude, longitude 两个字段，代表用户的经纬度位置
        // 2.3 将获取到的 经纬度传值给 getAddress 解析出 具体的地址
        that.data.params.guaranteeObject.lat = r.latitude
        that.data.params.guaranteeObject.lng = r.longitude
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})