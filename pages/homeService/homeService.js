// pages/homeService/homeService.js
import {
  homeService,
  listByProject,
  saveServiceTime,
  deleteByServiceUrnAndAndUserUrn
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      count: 100,
      start: 0,
      serviceProject: ''
    },
    serviceList: [],
    date: '2016-09-01',
  },
  bindDateChange: function (e) {
    let userInfo = wx.getStorageSync('userInfo')
    let data = e.currentTarget.dataset.item
    let i = e.currentTarget.dataset.index
    let parentIndex = e.currentTarget.dataset.parents
    if(data.isBook){
      let params={
        serviceUrn: data.serviceUrn,
        userUrn: userInfo.userUrn,
      }
      deleteByServiceUrnAndAndUserUrn(params).then(res=>{
        if (res.data.result) {
          wx.showToast({
            title: '取消预约成功',
            duration: 1500,
            icon: 'none',
          })
          // wx.redirectTo({
          //   url: '/pages/homeService/homeService',
          // })
          this.data.serviceList[parentIndex].homeServices[i].isBook =!this.data.serviceList[parentIndex].homeServices[i].isBook
          this.setData({
            serviceList:this.data.serviceList
          })
        } else {
          wx.showToast({
            title: res.data.error.message,
            duration: 1500,
            icon: 'none',
          })
        }
      })
    }else{
      let params = {
        districtId: userInfo.districtId,
        serviceUrn: data.serviceUrn,
        userUrn: userInfo.userUrn,
      }
      saveServiceTime(params).then(res => {
        if (res.data.result) {
          wx.showToast({
            title: '预约成功',
            duration: 1500,
            icon: 'none',
          })
          // wx.redirectTo({
          //   url: '/pages/homeService/homeService',
          // })
          this.data.serviceList[parentIndex].homeServices[i].isBook =!this.data.serviceList[parentIndex].homeServices[i].isBook
          this.setData({
            serviceList:this.data.serviceList
          })
        } else {
          wx.showToast({
            title: res.data.error.message,
            duration: 1500,
            icon: 'none',
          })
        }
      })
      // this.setData({
      //   date: e.detail.value
      // })
    }
  },
  getServeName(e) {
    this.data.params.serviceProject = e.detail.value
  },
  limitAgendaList() {
    this.gethomeService()
  },
  getserviceItem(e) {
    let data = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '/pages/serviceDetail/serviceDetail?id=' + data.serviceUrn,
      // url: '/pages/serviceList/serviceList?id=' + data.serviceUrn,

    })
  },
  clearTitle() {
    this.data.params.serviceProject = ''
    this.setData({
      params: this.data.params
    })
    this.gethomeService()
  },
  gethomeService() {
    listByProject(this.data.params).then(res => {
      this.setData({
        serviceList: res.data.result
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.districtId = userInfo.districtId
    this.data.params.userUrn = userInfo.userUrn
    this.gethomeService()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})