import {
  getsomeDatas,
  getArea,
  personReport
} from "../../request/getIndexData";

// pages/personReport/personReport.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showTopTips: false,

    radioItems: [{
        name: 'cell standard',
        value: '0',
        checked: true
      },
      {
        name: 'cell standard',
        value: '1'
      }
    ],
    checkboxItems: [{
        name: 'standard is dealt for u.',
        value: '0',
        checked: true
      },
      {
        name: 'standard is dealicient for u.',
        value: '1'
      }
    ],
    items: [{
        name: '否',
        value: '0'
      },
      {
        name: '是',
        value: '1',
        checked: 'true'
      },
    ],
    isAgree: false,
    formData: {

    },
    traffickList: [],
    trafficIndex: 1000,
    isStay: '0',
    isClose: '0',
    isKesou: '0',
    isQicu: '0',
    name: '',
    idcard: '',
    age: '',
    hujiaddress: '',
    mobile: '',
    originAddress: '',
    originDistrict: '',
    comeAddress: '',
    stayAddress: '',
    stayTime: '',
    flightNo: '',
    seatNo: '',
    temperature: '',
    multiArray: [],
    provinceArr: [],
    multiIndex: [0, 0, 0],
    receiverTime: '',
    mainDistricId: '',
    originTime: '请选择时间',
    checked: true,
    multiIndex2: [0, 0, 0],
    districtnName: '',
    originDistricId: '',
    origindistrictnName: '',
    originName: ''
  },
  //   bindTimeChange: function(e) {
  //     console.log('picker发送选择改变，携带值为', e.detail.value)
  //     this.setData({
  //       time: e.detail.value
  //     })
  //   },
  bindDateChange: function (e) {
    this.setData({
      originTime: e.detail.value + " 0:00:00"
    })
  },
  bindPickerChange: function (e) {
    let int = 0
    for (let i = 0; i < this.data.traffickList.length; i++) {
      if (e.detail.value == i) {
        int = this.data.traffickList[i].id
      }
    }
    this.setData({
      trafficIndex: e.detail.value,
      trafficId: int
    })
  },
  submitForm() {
    if (this.data.name && this.data.idcard && this.data.hujiaddress && this.data.mobile && this.data.originName && this.data.originAddress &&
      this.data.originTime && this.data.isStay && this.data.comeAddress && this.data.trafficId && this.data.flightNo && this.data.seatNo && this.data.isClose &&
      this.data.temperature && this.data.isKesou && this.data.isQicu && this.data.mainDistricId) {
      const params = {
        "name": this.data.name,
        "idCard": this.data.idcard,
        "address": this.data.hujiaddress,
        "mobile": this.data.mobile,
        "origin": this.data.originName,
        "districtId": this.data.mainDistricId,
        "originAddress": this.data.originAddress,
        "originTime": this.data.originTime,
        "stayStatus": this.data.isStay,
        "shanghaiAddress": this.data.comeAddress,
        "traffic": this.data.trafficId,
        "trafficNo": this.data.flightNo,
        "seatNo": this.data.seatNo,
        "receiverType": this.data.isClose,
        "temperature": this.data.temperature,
        "coughStatus": this.data.isKesou,
        "breathStatus": this.data.isQicu, //0否1是
        "outcome": this.data.districtnName,
        "outcomename": this.data.districtnName,
        "createName": wx.getStorageSync('userInfo').name
      }
      if (this.data.stayTime) {
        params.stayTime = this.data.stayTime
      }
      if (this.data.stayAddress) {
        params.stayAddress = this.data.stayAddress
      }
      if (this.data.receiverTime) {
        params.receiverTime = this.data.receiverTime
      }
      if (this.data.checked) {
        personReport(params).then(res => {
          if (res.data.error === null) {
            this.setData({
              success: '提交成功'
            })
            setTimeout(() => {
              wx.switchTab({
                url: '/pages/index/index'
              })
            }, 2000)
          }
        })
      } else {
        this.setData({
          error: '请勾选信息保证真实'
        })
      }
    } else {
      this.setData({
        error: '请填写必填项'
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getSomeData()
    getArea().then(res => {
      let provinceList = res.data.result
      let provinceArr = res.data.result.map((item) => {
        return item.district.name
      })
      this.setData({
        multiArray: [provinceArr, [],
          []
        ],
        provinceList,
        provinceArr
      })
      let defaultCode = this.data.provinceList[0].district.districtId // 使用第一项当作参数获取市级数据
      if (defaultCode) {
        this.setData({
          currnetProvinceKey: defaultCode // 保存在当前的省级key
        })
        this.getCity(defaultCode) // 获取市级数据
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindKeyInputname(e) {
    this.setData({
      name: e.detail.value
    })
  },
  bindKeyInputidcard(e) {
    this.setData({
      idcard: e.detail.value
    })
  },
  bindKeyInputage(e) {
    this.setData({
      age: e.detail.value
    })
  },
  bindKeyInputhujiaddress(e) {
    this.setData({
      hujiaddress: e.detail.value
    })
  },
  bindKeyInputmobile(e) {
    this.setData({
      mobile: e.detail.value
    })
  },
  bindKeyInputOriginaddress(e) {
    this.setData({
      originAddress: e.detail.value
    })
  },
  bindKeyInputOriginCity(e) {
    this.setData({
      originName: e.detail.value
    })
  },
  bindKeyInputOriginDistrict(e) {
    this.setData({
      originDistrict: e.detail.value
    })
  },
  bindKeyInputOrigintime(e) {
    this.setData({
      originTime: e.detail.value
    })
  },
  bindKeyInputComeAddress(e) {
    this.setData({
      comeAddress: e.detail.value
    })
  },
  bindKeyInputstayAddress(e) {
    this.setData({
      stayAddress: e.detail.value
    })
  },
  bindKeyInputstayTime(e) {
    this.setData({
      stayTime: e.detail.value
    })
  },
  bindKeyInputflightNo(e) {
    this.setData({
      flightNo: e.detail.value
    })
  },
  bindKeyInputseatNo(e) {
    this.setData({
      seatNo: e.detail.value
    })
  },
  bindKeyInputtemperature(e) {
    this.setData({
      temperature: e.detail.value
    })
  },
  getSomeData() {
    getsomeDatas({
      dictGroup: 'TRAFFIC'
    }).then(res => {
      this.setData({
        traffickList: res.data.results
      })
    })
  },
  adioChangeOfStay(e) {
    const items = this.data.items
    for (let i = 0, len = items.length; i < len; ++i) {
      items[i].checked = items[i].value === e.detail.value
    }
    this.setData({
      items,
      isStay: e.detail.value
    })
  },
  radioChangeOfClose(e) {
    const items = this.data.items
    for (let i = 0, len = items.length; i < len; ++i) {
      items[i].checked = items[i].value === e.detail.value
    }
    this.setData({
      items,
      isClose: e.detail.value
    })
  },
  radioChangeOfKesou(e) {
    const items = this.data.items
    for (let i = 0, len = items.length; i < len; ++i) {
      items[i].checked = items[i].value === e.detail.value
    }
    this.setData({
      items,
      isKesou: e.detail.value
    })
  },
  radioChangeOfQicu(e) {
    const items = this.data.items
    for (let i = 0, len = items.length; i < len; ++i) {
      items[i].checked = items[i].value === e.detail.value
    }
    this.setData({
      items,
      isQicu: e.detail.value
    })
  },
  bindMultiPickerChange: function (e) {
    this.setData({
      multiIndex: e.detail.value,
      mainDistricId: this.data.storeList[e.detail.value[2]].district.districtId,
      districtnName: this.data.storeList[e.detail.value[2]].district.name,
    })
  },
  // bindMultiPickerChange2: function (e) {
  //   this.setData({
  //     multiIndex2: e.detail.value,
  //     originDistricId: this.data.storeList[e.detail.value[2]].district.districtId,
  //     origindistrictnName: this.data.storeList[e.detail.value[2]].district.name,
  //   })
  // },
  changedate(e) {
    if (e.currentTarget.dataset.checked) {
      this.setData({
        checked: false
      })
    } else {
      this.setData({
        checked: true
      })
    }
  },
  getCity(code) {
    this.data.provinceList.forEach(item => {
      let cityList = []
      if (item.district.districtId === code) {
        cityList = [...item.children]
      }
      let cityArr = cityList.map((item) => {
        return item.district.name
      })
      this.setData({
        multiArray: [this.data.provinceArr, cityArr, []], // 更新三维数组 更新后长这样 [['江苏省', '福建省'], ['徐州市'], []]
        cityList, // 保存下市级原始数据
        cityArr // 市级所有的名称
      })
      let defaultCode = this.data.cityList[0].district.districtId // 用第一个获取门店数据
      if (defaultCode) {
        this.setData({
          currnetCityKey: defaultCode // 存下当前选择的城市key
        })
        this.getStore(defaultCode) // 获取门店数据
      }
    })
  },
  getStore(code) {
    this.setData({
      currnetCityKey: code // 更新当前选择的市级key
    })
    let storeList = []
    this.data.cityList.forEach(item => {
      if (item.district.districtId === code) {
        storeList = [...item.children]
        let storeArr = storeList.map((item) => {
          return item.district.name
        })
        this.setData({
          multiArray: [this.data.provinceArr, this.data.cityArr, storeArr],
          storeList,
          storeArr
        })
      }
    })
  },
})