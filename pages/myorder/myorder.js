// pages/myorder/myorder.js
import {
    getMyOrder,
    cancelOrder
} from '../../request/getIndexData'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        pageType: 5,
        showNothing: false,
        myOrderList: [],
        pagination: {
            totalPage: 0,
            start: 0,
            count: 10,
            page: 1,
        }
    },
    handleCancelOrder: function (e) {
        let d = e.currentTarget.dataset.slice
        cancelOrder({
            serviceUrn: d.serviceUrn,
            userUrn: wx.getStorageSync('userInfo').userUrn
        }).then(res => {
            if (res.data.error == null) {
                wx.showToast({
                    title: '取消成功',
                })
            } else {
                wx.showToast({
                    title: '取消失败'
                })
            }
            this.setData({
                myOrderList: []
            })
            this.getMyOrderList()
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    onReachBottom: function () {
        let page = this.data.pagination.page + 1
        if (page <= this.pagination.totalPage) {
            this.setData({
                pagination: {
                    start: (page - 1) * this.data.pagination.count,
                    count: this.data.pagination.count,
                    page: page
                }
            })
        }
    },
    getMyOrderList: function () {
        getMyOrder({
            start: this.data.pagination.start,
            count: this.data.pagination.count,
            workerUrn: wx.getStorageSync('userInfo').userUrn
        }).then(res => {
            if (res.data.pagination) {
                this.setData({
                    showNothing: false
                })
                this.setData({
                    pagination: {
                        start: res.data.pagination.start,
                        count: res.data.pagination.count,
                        total: res.data.pagination.total,
                        page: this.data.pagination.page,
                        totalPage: Math.ceil(res.data.pagination.total / this.data.pagination.count)
                    }
                })
                let myOrderList = this.data.myOrderList.concat(res.data.results)
                this.setData({
                    myOrderList: myOrderList
                })
            } else {
                this.setData({
                    showNothing: true
                })
            }

        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.setData({
            myOrderList: []
        })
        this.getMyOrderList()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})