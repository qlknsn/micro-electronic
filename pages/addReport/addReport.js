// pages/addReport/addReport.js
import {
  dictionaryList,
  saveHandle
  
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    files: [],
    userReceivedImgList: [],
    EventList: [],
    index: 0,
    villageList: [],
    villageIndex: 0,
    address: {},
    // descrtion:'',
    params: {
      handleObject:{},
      filePaths:[]
    }
  },
  handleDeleteImg: function (params) {
    let d = params.currentTarget.dataset.index
    let list = this.data.files
    let receiveList = this.data.userReceivedImgList
    list.splice(d, 1)
    receiveList.splice(d, 1)
    this.setData({
      files: list
    })
    this.setData({
      userReceivedImgList: receiveList
    })
  },
  chooseImage() {
    var that = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album','camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        if (that.data.files.length >= 3) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传三张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths
          let temp = that.data.files
          temp.push(tempFilePaths[0])
          that.setData({
            files: [...temp]
          })
          wx.uploadFile({
            filePath: tempFilePaths[0],
            name: 'file',
            url: `https://micro-electronic.bearhunting.cn/public/api/hip/v1/attachment/fileUpload`,
            header: {
              "Content-Type": "multipart/form-data",
              "token": wx.getStorageSync('token')
            },
            success: function (res) {
              if (res.statusCode == 200) {
                let d = JSON.parse(res.data).result
                let temp = that.data.userReceivedImgList
                temp.push(d[0])
                that.setData({
                  userReceivedImgList: temp
                })
              } else {
                wx.showModal({
                  title: '警告',
                  content: "上传失败，请重试",
                  showCancel: false
                })
                let temp = that.data.files
                temp.pop()
                that.setData({
                  files: [...temp]
                })
              }
            }
          })
        }

        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        // that.setData({
        //   files: that.data.files.concat(res.tempFilePaths)
        // });
      }
    })
  },
  submitBtn() {
    this.data.params.filePaths = this.data.userReceivedImgList
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.handleObject.mobile = userInfo.mobile
    this.data.params.handleObject.name = userInfo.nickname
    this.data.params.handleObject.portraitImg = userInfo.portraitImg
    saveHandle(this.data.params).then(res => {
      if(res.data.error){
        wx.showToast({
          title: '添加随手报',
        })
      }else{
        wx.reLaunch({
          url: '/pages/reportList/reportList',
        })
      }
    })
  },
  getdictionary() {
    dictionaryList({
      dictGroup: 'EVENT'
    }).then(res => {
      this.setData({
        EventList: res.data.results
      })
    })
  },

  bindPickerChange(e) {
    this.setData({
      index: e.detail.value
    })
    this.data.params.handleObject.event = this.data.EventList[this.data.index].code
  },
  bindVillageChange(e) {
    this.setData({
      villageIndex: e.detail.value
    })
    this.data.params.handleObject.districtId = this.data.villageList[this.data.villageIndex].districtId
    this.data.params.handleObject.districtName = this.data.villageList[this.data.villageIndex].districtName
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getLocation({
      success: function (res) {
      }
    })

    this.getdictionary()
    let userInfo = wx.getStorageSync('userInfo')
    let arr = []
    if (userInfo.deputyDistrictId) {
      arr = [{
          districtId: userInfo.districtId,
          districtName: userInfo.districtName
        },
        {
          districtId: userInfo.deputyDistrictId,
          districtName: userInfo.deputyDistrictName
        }
      ]
    } else {
      arr = [{
        districtId: userInfo.districtId,
        districtName: userInfo.districtName
      }, ]
    }
    this.setData({
      villageList: arr
    })
  },
  getDexcription(e) {
    this.data.params.handleObject.content = e.detail.value
  },
  getLO() {
    let that = this
    wx.getLocation({
      success: function (res) {
        wx.chooseLocation({
          latitude: res.latitude,
          longitude: res.longitude,
          success: function (res) {
            that.setData({
              address: res
            })
            that.data.params.handleObject.address = res.name
          }
        })
      }
    })
  },
  createMap() {
    let that = this
    wx.getSetting({
      success: function (res) {

        if (res.authSetting['scope.userLocation']) {
          that.getLO()
        } else {
          wx.openSetting({
            success: function (res) {
              // if(res)
              if (res.authSetting['scope.userLocation']) {
                that.getLO()
              }
            }
          })
        }
      }
    })

    // wx.chooseLocation
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})