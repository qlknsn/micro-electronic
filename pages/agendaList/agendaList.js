// import { from } from "form-data";
// pages/agendaList/agendaList.js
import {
  getAssemblyAll
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    agendaList: [],
    params: {
      start: 0,
      count: 10,
      districtId: '',
      title: '',
      userUrn: ''
    },
    upCount: 1,
    showLimit: false
  },
  getTitle(e) {
    this.data.params.title = e.detail.value
    this.setData({
      agendaList: [],
      upCount: 1
    })
    this.getAssemblyAlls(this.data.upCount)
  },
  addAgenda() {
    wx.navigateTo({
      url: '/pages/sendTrend/sendTrend',
    })
  },

  getAssemblyAlls(index) {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.districtIds = userInfo.districtId
    this.data.params.userUrn = userInfo.userUrn
    this.data.params.start = (index - 1) * this.data.params.count
    getAssemblyAll(this.data.params).then(res => {
      if (res.data.results.length == 10) {
        let arr = [...this.data.agendaList,...res.data.results]
        this.setData({
          agendaList: arr
        })
      } else if (res.data.results.length == 0) {
        this.setData({
          showLimit: true
        })
      } else {
        let arr = [...this.data.agendaList,...res.data.results]
        this.setData({
          agendaList: arr
        })
        this.setData({
          showLimit: true
        })
      }

    })
  },
  limitAgendaList() {
    this.data.upCount = 1
    this.getAssemblyAlls(this.data.upCount)
  },
  clearTitle() {
    this.data.params.title = ''
    this.setData({
      params: this.data.params
    })
    this.data.upCount = 1
    this.getAssemblyAlls(this.data.upCount)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAssemblyAlls(this.data.upCount)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.upCount++
    this.getAssemblyAlls(this.data.upCount)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})