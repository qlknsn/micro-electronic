// pages/vote/vote.js
import { doVote,voteDetail } from '../../request/getIndexData'

import {baseUrl} from '../../request/publicData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail:{

    },
    items:[],
    selectItems: [ 
    ],
    pickingType:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      console.log(options.id);
      // this.getVoteSelectList(options.id);
      this.getDetail(options.id);
  },
  moreVote(e){
    console.log(e.currentTarget.dataset)
    let currentValue = e.currentTarget.dataset.value;

    let tempItems = this.data.detail.voteList;
    for(let i = 0;i < tempItems.length;i++){
      if(tempItems[i].optionName == currentValue){
        tempItems[i].voted = !tempItems[i].voted
      }
    }
    this.setData({
      items:tempItems
    });
    let arr = [];
    this.data.items.forEach(item =>{
      if(item.voted){
        arr.push(item.optionName)
      }
    })
    this.setData({
      selectItems:arr
    })
    console.log(this.data.items)
  },

  getDetail(id){
    let params = {
      openid:wx.getStorageSync('openId'),
      homePageThrId:id
    }
    voteDetail(params).then(res =>{
      let temparr = res.data.result.voteList;
      let arr = [];
      for(let i = 0;i < temparr.length;i++){
        if(temparr[i].voted){
          arr.push(temparr[i].optionName);
        }
      }
      this.setData({
        detail:res.data.result,
        items:res.data.result.voteList,
        selectItems:arr
      })
    });


  },

  getVoteSelectList(id){  //获取投票列表
    let that = this
    let token = wx.getStorageSync('token')
    let header = {
      token: token
    }
    wx.request({
      url: baseUrl+'/public/api/hip/v1/voteTable/find/'+id,
      // url: baseUrl+'/public/api/hip/v1/voteTable/find/'+"43",
      method:"get",
      header:header,
      success(res){
        console.log(res.data.result)
        that.setData({
          detail:res.data.result,
        });
        if(res.data.result.pickingType == '多选'){
          let options = res.data.result.options;
          let arr = []
          for(let i = 0;i <options.length;i++ ){
            let obj = {}
            obj.checked = false
            obj.value = options[i]
            arr.push(obj)
          }
          that.setData({
            items:arr
          });
        };

      }
    })
  },
  radioChange(e){
    console.log(e.detail);
    let arr = [];
    arr.push(e.detail.value);
    this.setData({
      selectItems:arr
    })
  },
  submit(){
    if(this.data.selectItems.length <= 0){
      wx.showToast({
        title: '请勾选',
      })
      return
    }
    let params = {
      openid:wx.getStorageSync('openId'),
      opinions:this.data.selectItems,
      placardId:this.data.detail.placardId
    }
    if(this.data.pickingType){
      let arr = [];
      // let selected = this.data.items;
      this.data.items.forEach(item =>{
          if(item.checked){
            arr.push(item.value);
            console.log(arr)
          }
      });
      params.options = arr;
    }
    doVote(params).then(res =>{
      if(res.data.errors.length <= 0){
        wx.showToast({
          title: '投票完成',
        });
        setTimeout(()=>{
          wx.navigateBack({
            delta: 0,
          })
        },500)
        
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})