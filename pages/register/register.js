// pages/register/register.js
const app = getApp();
import {
  getArea,
  getsomeDatas,
  personregister,
  jscode2session,
  loginByOpenid,
  getPhoneNumber
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    policyCheckBox: false,
    showTopTips: false,
    phone: '',
    nickname: '',
    mainAddress: '',
    secAddress: '',
    name: '',
    idcard: '',
    registerAddress: '',
    mainDistricId: null,
    secDistricId: null,
    multiIndex: [0, 0, 0],
    multiIndex111: [0, 0, 0],
    multiArray: [],
    items: [],
    districtnName: '',
    date: "2016-09-01",
    time: "12:01",
    countryCodeIndex: 0,
    isAgree: false,
    provinceArr: [],
    nationList: [],
    nationIndex: 1000,
    currentnation: '',
    reasonList: [],
    reasonIndex: 1000,
    currentreason: '',
    persontype: '',
    openId: '',
    isChinesename: false,
    menhao: '',
    menshi: '',
    phoneNumber: '',

  },
  checkboxChange: function (e) {
    console.log(e.detail.value)
    e.detail.value.length > 0 ? this.setData({
      policyCheckBox: true
    }) : this.setData({
      policyCheckBox: false
    })
  },
  redirectUserServicePolicy: function (e) {
    console.log(e.target.dataset.type)
    wx.navigateTo({
      url: '/pages/policy/policy?type=' + e.target.dataset.type,
    })
  },
  getPhoneNumber(e) {
    let errMsg = e.detail.errMsg;
    if (errMsg.indexOf('ok') > -1) {
      getPhoneNumber({
        encryptedData: e.detail.encryptedData,
        iv: e.detail.iv,
        sessionKey: wx.getStorageSync('sessionKey')
      }).then(res => {
        this.setData({
          phone: res.data.result.phoneNumber
        })
      })
    } else {
      wx.showToast({
        title: '请授权',
        icon: 'none'
      })
    }
  },
  radioChange(e) {
    const items = this.data.items
    for (let i = 0, len = items.length; i < len; ++i) {
      items[i].checked = items[i].code === e.detail.value
    }
    this.setData({
      items,
      persontype: e.detail.value
    })
  },
  bindKeyInputphone(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  bindKeyInputnickname(e) {
    this.setData({
      nickname: e.detail.value
    })
  },
  bindKeyInputmainaddress(a, b) {
    this.setData({
      mainAddress: a + b
    })
  },
  getmenhao(e) {
    this.setData({
      menhao: e.detail.value
    })
    this.bindKeyInputmainaddress(this.data.menhao, this.data.menshi)
  },
  getmenshi(e) {
    this.setData({
      menshi: e.detail.value
    })
    this.bindKeyInputmainaddress(this.data.menhao, this.data.menshi)
  },
  bindKeyInputsecaddress(e) {
    this.setData({
      secAddress: e.detail.value
    })
  },
  bindKeyInputname(e) {
    this.setData({
      name: e.detail.value
    })
    this.isChinese(e.detail.value)
  },
  isChinese(val) {
    let reg = /^[\u4e00-\u9fa5]+$/i;
    if (reg.test(val)) {
      this.setData({
        isChinesename: true
      })
    } else {
      this.setData({
        isChinesename: false
      })
    }
  },
  bindKeyInputidcard(e) {
    this.setData({
      idcard: e.detail.value
    })
  },
  bindKeyInputregisterAddress(e) {
    this.setData({
      registerAddress: e.detail.value
    })
  },
  bindMultiPickerChange: function (e) {
    this.setData({
      multiIndex: e.detail.value
    })
    this.setData({
      mainDistricId: this.data.storeList[e.detail.value[2]].district.districtId,
      districtnName: this.data.storeList[e.detail.value[2]].district.name,
    })
  },
  bindMultiPickerChange111: function (e) {
    this.setData({
      multiIndex111: e.detail.value
    })
    this.setData({
      secDistricId: this.data.storeList[e.detail.value[2]].district.districtId,
      deputyDistrictnName: this.data.storeList[e.detail.value[2]].district.name,
    })
  },
  getopenId(code) {
    return new Promise((resolve, reject) => {
      jscode2session({
        jsCode: code
      }).then(res => {
        wx.setStorageSync('sessionKey', res.data.result.sessionKey)
        resolve(res.data.result.openid)
      })
    })
  },
  login(openid) {
    loginByOpenid({
      openid: openid
    }).then(res => {
      if (res.data.result.residentMenu) {
        wx.setStorageSync('userInfo', res.data.result.residentObject)
        wx.setStorageSync('residentMenu', res.data.result.residentMenu)
        wx.setStorageSync('token', res.data.result.token)
        app.globalData.tabList = [{
            "pagePath": "/pages/index/index",
            "iconPath": "../image/home.png",
            "selectedIconPath": "../image/shouye@2x.png",
            "text": "首页"
          },
          {
            "pagePath": "/pages/help/help",
            "iconPath": "../image/qiuzhu@2x.png",
            "selectedIconPath": "../image/qiuzhu@2x.png",
            "text": "一键求助"
          },
          {
            "pagePath": "/pages/my/my",
            "iconPath": "../image/wode1@2x.png",
            "selectedIconPath": "../image/wode@2x.png",
            "text": "我的"
          }
        ]
        // setTimeout(()=>{
        wx.reLaunch({
          url: '/pages/index/index',
        })
      } else {
        wx.clearStorage()
        wx.reLaunch({
          url: '/pages/register/register',
        })
      }
    })
  },
  judgeIDcardV2() { // 1 "验证通过!", 0 //校验不通过
    let idCard = this.data.idcard
    var format = /^(([1][1-5])|([2][1-3])|([3][1-7])|([4][1-6])|([5][0-4])|([6][1-5])|([7][1])|([8][1-2]))\d{4}(([1][9]\d{2})|([2]\d{3}))(([0][1-9])|([1][0-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))\d{3}[0-9xX]$/;
    //号码规则校验
    if (!format.test(idCard)) {
      return false
    }
    //区位码校验
    //出生年月日校验   前正则限制起始年份为1900;
    var year = idCard.substr(6, 4), //身份证年
      month = idCard.substr(10, 2), //身份证月
      date = idCard.substr(12, 2), //身份证日
      time = Date.parse(month + '-' + date + '-' + year), //身份证日期时间戳date
      now_time = Date.parse(new Date()), //当前时间戳
      dates = (new Date(year, month, 0)).getDate(); //身份证当月天数
    if (time > now_time || date > dates) {
      return false
    }
    //校验码判断
    var c = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //系数
    var b = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'); //校验码对照表
    var id_array = idCard.split("");
    var sum = 0;
    for (var k = 0; k < 17; k++) {
      sum += parseInt(id_array[k]) * parseInt(c[k]);
    }
    if (id_array[17].toUpperCase() != b[sum % 11].toUpperCase()) {
      return false
    }
    return true
  },
  // 身份证校验
  judgeIDcard() {
    let idCard = this.data.idcard
    var sex = idCard.substring(16, 17);
    var num = idCard.substring(17, 18);
    var year = idCard.substring(6, 10);
    var month = idCard.substring(10, 12);
    var day = idCard.substring(12, 14);
    var age = year + "-" + month + "-" + day;
    var date = new Date(age);
    var sum = 0;
    var arr = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 0];
    var arr2 = [1, 0, "X", "x", 9, 8, 7, 6, 5, 4, 3, 2];
    //判断身份证是否为空，或者长度有没有达到要求(这里规定身份证18位)
    if (!(idCard && /^[\d]{18}$/.test(idCard))) {
      return false;
    } else {
      //计算第18位数 是否符合要求
      for (var i = 0; i < 18; i++) {
        sum += idCard.substring(i, i + 1) * arr[i];
      }
      if (num == arr2[sum % 11] || num === arr2[sum % 11]) {
        return true
      } else {
        return false;
      }
    }
  },
  submitForm() {
    wx.showLoading({
      title: '注册中请稍等',
    })
    if (!this.data.policyCheckBox) {
      wx.hideLoading({
        success: (res) => {},
      })
      wx.showToast({
        title: '请勾选协议',
      })
      return
    }
    // 身份证校验
    if (!this.judgeIDcardV2()) {
      wx.showToast({
        title: '身份证号错误',
        icon: 'none'
      })
      return
    }
    if (this.data.phone.length !== 11) {
      wx.hideLoading()
      wx.showToast({
        title: '手机号必须是11位',
        icon: 'none'
      })
      return false;
    }
    if (!this.data.registerAddress) {
      wx.showToast({
        title: '户籍必填',
      })
    }
    let that = this
    if (this.data.mainAddress && this.data.mainDistricId && this.data.persontype && this.data.nickname && this.data.name && this.data.currentreason && this.data.phone) {
      if (this.data.isChinesename) {
        wx.showLoading({
          title: '注册中请稍等',
        })
        const params = {
          "address": this.data.mainAddress,
          "deputyAddress": this.data.secAddress,
          "deputyDistrictId": this.data.secDistricId,
          "deputyDistrictnName": this.data.deputyDistrictnName,
          "districtId": this.data.mainDistricId,
          "districtName": this.data.districtnName,
          "identityCard": this.data.idcard,
          "mobile": this.data.phone,
          "name": this.data.name,
          "nation": this.data.currentnation,
          "nickname": this.data.nickname,
          "openid": this.data.openId,
          "personType": this.data.persontype,
          "reason": this.data.currentreason,
          "registerAddress": this.data.registerAddress,
        }
        personregister(params).then(res => {
          if (res.data.error == null) {
            wx.login({
              success(res) {
                that.getopenId(res.code).then(openid => {
                  wx.setStorageSync('openId', openid)
                  that.login(openid)
                })

              }
            })
            wx.setStorageSync('peopleType', 'person')

          } else {
            wx.hideLoading()
            wx.showModal({
              title: '警告',
              content: res.data.error.message
            })
          }
        })
      } else {
        this.setData({
          error: '需填写中文姓名'
        })
        wx.hideLoading({
          success: (res) => {},
        })
      }
    } else {
      wx.hideLoading({
        success: (res) => {},
      })
      this.setData({
        error: '请填写必填项'
      })
    }
  },
  getDatas(name) {
    getsomeDatas({
      dictGroup: name
    }).then(res => {
      switch (name) {
        case 'NATION':
          this.setData({
            nationList: res.data.results
          })
          break;
        case 'LIVE_REASION':
          this.setData({
            reasonList: res.data.results
          })
          break;
        case 'USER_TYPE':
          this.setData({
            items: res.data.results
          })
          break;
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getDatas('NATION')
    this.getDatas('LIVE_REASION')
    this.getDatas('USER_TYPE')
    getArea().then(res => {
      let provinceList = res.data.result
      let provinceArr = res.data.result.map((item) => {
        return item.district.name
      })
      this.setData({
        multiArray: [provinceArr, [],
          []
        ],
        provinceList,
        provinceArr
      })
      let defaultCode = this.data.provinceList[0].district.districtId // 使用第一项当作参数获取市级数据
      if (defaultCode) {
        this.setData({
          currnetProvinceKey: defaultCode // 保存在当前的省级key
        })
        this.getCity(defaultCode) // 获取市级数据
      }
    })
  },
  getCity(code) {
    this.data.provinceList.forEach(item => {
      let cityList = []
      if (item.district.districtId === code) {
        cityList = [...item.children]
      }
      let cityArr = cityList.map((item) => {
        return item.district.name
      })
      this.setData({
        multiArray: [this.data.provinceArr, cityArr, []], // 更新三维数组 更新后长这样 [['江苏省', '福建省'], ['徐州市'], []]
        cityList, // 保存下市级原始数据
        cityArr // 市级所有的名称
      })
      let defaultCode = this.data.cityList[0].district.districtId // 用第一个获取门店数据
      if (defaultCode) {
        this.setData({
          currnetCityKey: defaultCode // 存下当前选择的城市key
        })
        this.getStore(defaultCode) // 获取门店数据
      }
    })
  },
  getStore(code) {
    this.setData({
      currnetCityKey: code // 更新当前选择的市级key
    })
    let storeList = []
    this.data.cityList.forEach(item => {
      if (item.district.districtId === code) {
        storeList = [...item.children]
        let storeArr = storeList.map((item) => {
          return item.district.name
        })
        this.setData({
          multiArray: [this.data.provinceArr, this.data.cityArr, storeArr],
          storeList,
          storeArr
        })
      }
    })
  },
  bindPickerChange: function (e) {
    let int = 0
    for (let i = 0; i < this.data.nationList.length; i++) {
      if (e.detail.value == i) {
        int = this.data.nationList[i].code
      }
    }
    this.setData({
      nationIndex: e.detail.value,
      currentnation: int
    })
  },
  bindPickerChange111: function (e) {
    let int = 0
    for (let i = 0; i < this.data.reasonList.length; i++) {
      if (e.detail.value == i) {
        int = this.data.reasonList[i].code
      }
    }
    this.setData({
      reasonIndex: e.detail.value,
      currentreason: int
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    wx.login({
      success(res) {
        that.getopenId(res.code).then(openid => {
          that.data.openId = openid
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})