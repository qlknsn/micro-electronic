// pages/sendTrend/sendTrend.js
import {
  saveAssembly
} from '../../request/getIndexData'
import {
  baseUrl
} from '../../request/publicData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    descriptionText: '',
    textLength: 0,
    files: [],
    userReceivedImgList: [],
    params: {
      assemblyObject: {
        content: '',
        districtId: '',
        neighbourhood: '',
        mobile: '',
        name: '',
        title: ''
      },
      filePaths: []
    }
  },
  getTitle(e) {
    this.data.params.assemblyObject.title = e.detail.value
    this.setData({
      params: this.data.params
    })

  },
  bindTextAreaBlur(e) {
    // console.log(e.detail.value.length)
    if (e.detail.value.length == 501) {
      wx.showToast({
        title: '最多只能输入500字',
      })
    } else {
      this.setData({
        textLength: e.detail.value.length
      })
      this.data.params.assemblyObject.content = e.detail.value
      this.setData({

      })
    }

  },
  handleDeleteImg: function (params) {
    let d = params.currentTarget.dataset.index
    let list = this.data.files
    let receiveList = this.data.userReceivedImgList
    list.splice(d, 1)
    receiveList.splice(d, 1)
    this.setData({
      files: list
    })
    this.setData({
      userReceivedImgList: receiveList
    })
  },
  chooseImage() {
    var that = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        if (res.tempFilePaths.length + that.data.files.length > 3) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传三张图片",
            showCancel: false
          })
        } else {
          let tempFilePaths = res.tempFilePaths
          let temp = that.data.files
          tempFilePaths.forEach(item => {
            temp.push(item)
            that.setData({
              files: [...temp]
            })
            wx.uploadFile({
              filePath: item,
              name: 'file',
              url: baseUrl + '/public/api/hip/v1/attachment/fileUpload',
              header: {
                "Content-Type": "multipart/form-data",
                "token": wx.getStorageSync('token')
              },
              success: function (res) {
                if (res.statusCode == 200) {
                  let d = JSON.parse(res.data).result
                  let temp = that.data.userReceivedImgList
                  temp.push(d[0])
                  that.setData({
                    userReceivedImgList: temp
                  })
                } else {
                  wx.showModal({
                    title: '警告',
                    content: "上传失败，请重试",
                    showCancel: false
                  })
                  let temp = that.data.files
                  temp.pop()
                  that.setData({
                    files: [...temp]
                  })
                }
              },
              fail: err => {
                console.log(err)
              }
            })
          })

        }
      }
    })
  },
  submitIt() {
    this.data.params.filePaths = this.data.userReceivedImgList
    saveAssembly(this.data.params).then(res => {
      if (res.data.error) {
        wx.showToast({
          title: '添加失败',
        })
      } else {
        wx.showToast({
          title: '添加成功',
          success: res => {
            setTimeout(() => {
              wx.switchTab({
                url: '/pages/index/index',
              })
            }, 2000);

          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.assemblyObject.districtId = userInfo.districtId
    this.data.params.assemblyObject.neighbourhood = userInfo.districtName
    this.data.params.assemblyObject.mobile = userInfo.mobile
    this.data.params.assemblyObject.name = userInfo.nickname
    this.data.params.assemblyObject.portraitImg = userInfo.portraitImg
    this.setData({
      params: this.data.params
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})