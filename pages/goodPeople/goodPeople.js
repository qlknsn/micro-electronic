// pages/goodPeople/goodPeople.js
import {
  listByType
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      count: 1000,
      start: 0,
      title: '',
      districtIds: '',
      type: ''
    },
    shequList: [],
    IMG_HOST: 'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/'
  },
  getTitle(e) {
    console.log(e.detail.value)
    this.data.params.title = e.detail.value
    this.getListByType(this.data.params)
  },
  searchNotice() {
    this.getListByType()
  },
  cancelNotice() {
    this.data.params.title = ''
    this.setData({
      params: this.data.params
    })
    this.getListByType()
  },
  getListByType() {
    listByType(this.data.params).then(res => {
      this.setData({
        shequList: res.data.results
      })
    })
  },
  getNoticeDetail(e) {
    let item = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '/pages/goodPeopleDetail/goodPeopleDetail?id=' + item.id + '&type=' + item.type,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.districtIds = userInfo.districtId
    this.data.params.type = options.type

    if (options.type == '1') {
      wx.setNavigationBarTitle({
        title: '失物招领'
      })
    } else {
      wx.setNavigationBarTitle({
        title: '好人好事'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getListByType(this.data.params)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})