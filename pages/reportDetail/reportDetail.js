// pages/reportDetail/reportDetail.js
import {findAssemblyByUniqueId} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reportDetail:{},
    IMG_HOST:'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/'
  },
  getReportDetail(id){
    findAssemblyByUniqueId({uniqueId:id}).then(res=>{
      this.setData({
        reportDetail:res.data.result.handleObject
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getReportDetail(options.uniqueId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})