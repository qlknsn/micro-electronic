// pages/reportList/reportList.js
import {
  listByTypeshb
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reportList:[],
    showLimit:false,
    upCount:1,
    params:{
      start:0,
      count:10,
      content:'',
      userUrn:'',
      districtIds:''
    },
  },
  getTitle(e){
    console.log(e)
    this.data.params.content = e.detail.value
  },
  addAgenda(){
    wx.navigateTo({
      url: '/pages/addReport/addReport',
    })
  },
  listByTypeshbs(index){
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.districtIds = userInfo.districtId
    this.data.params.userUrn = userInfo.userUrn
    this.data.params.start = (index-1)*this.data.params.count

    listByTypeshb(this.data.params).then(res=>{
      if(res.data.results.length==10){
        this.setData({
          reportList:res.data.results
        })
      }else if(res.data.results.length==0){
        this.setData({
          showLimit:true
        })
      }else{
        this.setData({
          reportList:res.data.results
        })
        this.setData({
          showLimit:true
        })
      }
      // this.setData({
      //   reportList:res.data.results
      // })
    })
  },
  limitReportList(){
    this.data.upCount = 1
    this.listByTypeshbs(this.data.upCount)
  },
  clearTitle(){
    this.data.params.content = ''
    this.setData({
      params:this.data.params
    })
    this.data.upCount = 1
    this.listByTypeshbs(this.data.upCount)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.listByTypeshbs(this.data.upCount)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.upCount++
    this.listByTypeshbs(this.data.upCount)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})