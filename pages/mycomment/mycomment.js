// pages/mycomment/mycomment.js
import {getCommentList} from '../../request/getIndexData'
import { imgUrl } from '../../request/publicData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cometype: '',
    inputShowed: false,
    inputVal: "",
    isnothing: false,
    myComments: [],
    ismore: false,
    start: 0,
    imgUrl: imgUrl
  },
  search: function (value) {
    return new Promise((resolve, reject) => {
      this.setData({
        start: 0,
        myComments: []
      })
      this.getSomeComment(value)
        // setTimeout(() => {
        //     resolve([{text: '搜索结果', value: 1}, {text: '搜索结果2', value: 2}])
        // }, 200)
    })
  },
  clearinput() {
    this.setData({
      start: 0,
      myComments: []
    })
    this.getSomeComment('')
  },
  selectResult: function (e) {
  },
  getSomeComment(content = '') {
    let userInfo = wx.getStorageSync('userInfo')
    const params = {
      "content": content,
      "count": 10,
      "start": this.data.start,
      "userUrn": userInfo.userUrn
    }
    getCommentList(params).then(res => {
      if(res.data.results.length === 0 && res.data.pagination.start === 0) {
        this.setData({
          isnothing: true
        })
      } else {
        let ismore = false
        let start = this.data.start
        if(res.data.pagination.total > res.data.pagination.start){
          ismore = true
          start = this.data.start + 10
        } else {
          ismore = false
        }
        this.setData({
          myComments: [...this.data.myComments, ...res.data.results],
          ismore: ismore,
          start: start,
          isnothing: false
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      cometype: options.type
    })
    this.setData({
      search: this.search.bind(this)
    })
    this.getSomeComment()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // 先做是否加载完成的判断，然后进行获取数据以及拼接
    if(this.data.ismore) {
      this.getSomeComment()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})