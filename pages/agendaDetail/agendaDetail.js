// pages/agendaDetail/agendaDetail.js
import {
  findHandleByUniqueId,
  findTree
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    agendaDetail: {},
    IMG_HOST: 'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/',
    params: {
      refUrn: '',
      userName: '',
      userUrn: '',
      examineStatus: 1,
      pid: null
    },
    discussList: [],
    showDiscussList: false,
    showDiscussComponent: false,
    blogId: '',
  },
  handleShowDiscussComponent: function () {
    this.setData({
      showDiscussComponent: true
    })
  },
  hideDiscuss: function () {
    this.setData({
      showDiscussComponent: false
    })
  },
  getAgendaDetail(id) {
    findHandleByUniqueId({
      uniqueId: id
    }).then(res => {
      this.setData({
        agendaDetail: res.data.result
      })
    })
  },
  getDisscusstree() {
    findTree(this.data.params).then(res => {
      this.setData({
        discussList: res.data.results,
        showDiscussList: res.data.results.length > 0 ? true : false
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.refUrn = options.uniqueId
    // this.data.params.userName = userInfo.nickname
    this.data.params.userUrn = userInfo.userUrn
    this.getDisscusstree()
    this.getAgendaDetail(options.uniqueId)
    this.setData({
      blogId: options.uniqueId
    })
    this.setData({
      discussItemData: {
        type: 'origin',
        id: this.data.blogId,
        refUrn: this.data.blogId
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})