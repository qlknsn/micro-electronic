// pages/taskHandleDetail/taskHandleDetail.js
// import getTrackingrouterInfo from "../../service/getTrackingrouterInfo"
// import request from '../../service/http'
// import GetTrackingInfo from '../../service/getTrackingrouterInfo'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkItemList: [],
    taskInfo: {},
    iteminfo: '1412441',
    allInfo: {},
    isJump : false

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    if ('taskId' in options) {
      this.getTaskDetail(options.taskId);
    } else {
      wx.showToast({
        title: '未传入taskId',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // let d = new GetTrackingInfo('/pages/taskHandleDetail/taskHandleDetail', 'PAGE_ONSHOW', {
    //   phone: wx.getStorageSync('phone'),
    //   name:wx.getStorageSync('userinfo').name,
    //   lat: wx.getStorageSync('lat'),
    //   lng: wx.getStorageSync('lng'),
    // })
    // d.getTrackingInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //radioGroup  切换
  radioChange: function (e) {
    if (e.detail.value > 0) {
      // wx.navigateTo({
      //   url: '/pages/uploadPic/uploadPic',
      // })
    } else {

    }

  },
  /**
   * 正常按钮的点击事件
   */
  normalBtnOnClick(event) {
    this.savaCheckItemRecord(event.currentTarget.dataset.iteminfo);
  },

  //非正常按钮的点击事件
  unnormalBtnOnClick(event) {
    wx.navigateTo({
      url: '/pages/uploadPic/uploadPic?taskId=' + this.data.taskInfo.taskId + '&elementId=' + event.currentTarget.dataset.iteminfo.elementId +
        '&checkItemId=' + event.currentTarget.dataset.iteminfo.checkItemId + '&checkResult=1',
    })
  },


  /**
   * 获取任务明细
   */
  getTaskDetail(taskId) {
    let params = {
      taskId: taskId
    }
    let p = new request('/url/task/v4/task/detail', params);
    p.get().then(res => {

      this.setData({
        checkItemList: res.data.data.current.processHistory,
        taskInfo: res.data.data.task,
        allInfo: res.data.data
      });

      //当stage==1时,拿到数据后循环保存每个检查内容,防止出现用户未选择现象
      if (res.data.data.task.stage != 2) {
        
      }
      this.data.checkItemList.forEach(item => {
        this.savaCheckItemRecord(item);
      })

    })
  },

  /**
   * 提交按钮
   */
  submit() {
    if (this.data.taskInfo.stage == 2) {
            let that = this

            let isHaveProblem = wx.getStorageSync("taskId"+that.data.allInfo.task.taskId);
            if(isHaveProblem){
                  let str = that.data.allInfo;
                  wx.navigateTo({
                    url: `/pages/department/department?taskId=${str.task.taskId}&checkItemCount=${str.current.processHistory.length}`,
                  })
            }else{
                  wx.getLocation({
                    success: function (res) {
                      that.handleTask(res)
                    }
                  })
            }
      
    } else {
      let that = this
      wx.getLocation({
        success: function (res) {
          that.handleTask(res)
        }
      })
    }

  },

  /**
   * 保存处置状态,记录每个checkitem选中的状态
   */
  savaCheckItemRecord(iteminfo) {
    let params = {
      taskId: this.data.taskInfo.taskId,
      elementId: iteminfo.elementId,
      checkItemId: iteminfo.checkItemId,
      checkResult: 0,
      present: 0
    };
    let p = new request('/url/task/v3/task/saveRecords', params);
    p.post().then(res => {

    })
  },

  /**
   * 处置任务
   */
  handleTask(res) {
    let params = {
      taskId: this.data.taskInfo.taskId,
      lat: res.latitude,
      lng: res.longitude,
      checkItemCount: this.data.checkItemList.length
    };
    let p = new request('/url/task/v4/task/handle', params);
    p.post().then(res => {

      if (res.data.code == 200) {
        wx.switchTab({
          url: '/pages/index/index'
        })
        wx.showToast({
          icon: "none",
          title: "处置完成",
        });

        //将保存在storge的任务是否无法处理的状态清楚
        wx.removeStorageSync('taskId'+this.data.taskInfo.taskId);
        // wx.setStorage({
        //   data: false,
        //   key: this.data.taskInfo.taskId,
        // })
      }
    })
  },


  /**
   * 获取检查内容item处置记录
   */
  getRecordsByCheckItemId(checkItemId){
        let params = {
          taskId:this.data.allInfo.task.taskId,
          checkItemId:checkItemId
        };
        let p = new request('/url/task/v3/task/getRecordsByCheckItemId',params);
        p.get().then(res =>{
              if(res.data.code == 200 && res.data.data.present == 1){
                   this.setData({
                     isJump:true,
                   });
              }else if(res.data.code == 200 && res.data.data.present == 0){
                    this.setData({
                      isJump:false,
                    });
              }
        })
  }

})