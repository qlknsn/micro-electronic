// pages/my/my.js
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showChanfePanel: false,
    userInfo: {},
    headUrl: '',
    showWuyeBar: false,
  },
  openchange() {
    this.setData({
      showChanfePanel: true
    })
  },
  closechange() {
    this.setData({
      showChanfePanel: false
    })
  },
  toPerson() {
    this.getTabBar().setData({
      list: [{
          "pagePath": "/pages/index/index",
          "iconPath": "../image/home.png",
          "selectedIconPath": "../image/shouye@2x.png",
          "text": "首页"
        },
        {
          "pagePath": "/pages/help/help",
          "iconPath": "../image/qiuzhu@2x.png",
          "selectedIconPath": "../image/qiuzhu@2x.png",
          "text": "一键求助"
        },
        {
          "pagePath": "/pages/my/my",
          "iconPath": "../image/wode1@2x.png",
          "selectedIconPath": "../image/wode@2x.png",
          "text": "我的"
        }
      ]
    });
    wx.setStorageSync('peopleType', 'person')
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  toWuye() {
    if (wx.getStorageSync('canWuye')) {
      this.getTabBar().setData({
        list: [{
            "pagePath": "/pages/historyFinish/historyFinish",
            "iconPath": "../image/home.png",
            "selectedIconPath": "../image/shouye@2x.png",
            "text": "首页"
          },
          {
            "pagePath": "/pages/my/my",
            "iconPath": "../image/wode1@2x.png",
            "selectedIconPath": "../image/wode@2x.png",
            "text": "我的"
          }
        ]
      });
      // this.getTabBar().setData({
      //   selected:0,
      // })
      wx.setStorageSync('peopleType', 'wuye')
      wx.reLaunch({
        url: '/pages/historyFinish/historyFinish',
      })
    } else {
      wx.showToast({
        title: '您没有物业权限',
        icon: 'none'
      })
    }

  },
  jumptoList(e) {
    let url = ''
    switch (e.currentTarget.dataset.id) {
      case '1':
        url = '/pages/myAgenda/myAgenda?type=1'
        break;
      case '2':
        url = '/pages/mycomment/mycomment?type=2'
        break;
      case '3':
        url = '/pages/repairs/repairs?type=3'
        break;
      case '4':
        url = '/pages/myvote/myvote'
        break;
      case '5':
        url = '/pages/myorder/myorder';
        break;
      case '6':
        url = '/pages/historyFinish/historyFinish'
        wx.setStorageSync('wuyeOrderStatus', "handling")
        break;
      case '7':
        wx.setStorageSync('wuyeOrderStatus', "finish")
        url = "/pages/historyFinish/historyFinish"
        break;

    }
    if (e.currentTarget.dataset.id == '6' || e.currentTarget.dataset.id == '7') {
      wx.switchTab({
        url: url,
      })
    } else {
      wx.navigateTo({
        url: url,
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo')
    let head = 'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/' + userInfo.portraitImg
    this.setData({
      userInfo: userInfo,
      headUrl: head
    })
    if (wx.getStorageSync('peopleType') == 'person') {
      this.getTabBar().setData({
        list: [{
            "pagePath": "/pages/index/index",
            "iconPath": "../image/home.png",
            "selectedIconPath": "../image/shouye@2x.png",
            "text": "首页"
          },
          {
            "pagePath": "/pages/help/help",
            "iconPath": "../image/qiuzhu@2x.png",
            "selectedIconPath": "../image/qiuzhu@2x.png",
            "text": "一键求助"
          },
          {
            "pagePath": "/pages/my/my",
            "iconPath": "../image/wode1@2x.png",
            "selectedIconPath": "../image/wode@2x.png",
            "text": "我的"
          }
        ]
      });
      // this.getTabBar().setData({
      //   selected: 2,
      // })
    } else {
      this.getTabBar().setData({
        list: [{
            "pagePath": "/pages/historyFinish/historyFinish",
            "iconPath": "../image/home.png",
            "selectedIconPath": "../image/shouye@2x.png",
            "text": "首页"
          },
          {
            "pagePath": "/pages/my/my",
            "iconPath": "../image/wode1@2x.png",
            "selectedIconPath": "../image/wode@2x.png",
            "text": "我的"
          }
        ]
      });

      // this.getTabBar().setData({
      //   selected: 1,
      // })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let type = wx.getStorageSync('peopleType')
    if (type == 'wuye') {
      this.setData({
        showWuyeBar: true
      })
      this.getTabBar().setData({
        selected: 1
      });
    } else {
      this.setData({
        showWuyeBar: false
      })
      this.getTabBar().setData({
        selected: 2
      });
    }
    let userInfo = wx.getStorageSync('userInfo')
    let head = 'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/' + userInfo.portraitImg
    this.setData({
      userInfo: userInfo,
      headUrl: head
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})