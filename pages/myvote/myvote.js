// pages/myvote/myvote.js
import { getMyVoteList } from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items:[]
  },
  getList(){
    let params = {
      openid:wx.getStorageSync('openId'),
      count:50,
      start:0
    }
    getMyVoteList(params).then(res =>{
      this.setData({
        items:res.data.results
      })
    })
  },
  onItemClick(e){
    console.log(e.currentTarget.dataset);
    if(e.currentTarget.dataset.type == '已结束'){
      wx.navigateTo({
        url: '/pages/voteDetail/voteDetail?id='+e.currentTarget.dataset.id,
      })
    }else{
      wx.navigateTo({
        url: '/pages/vote/vote?id='+e.currentTarget.dataset.id,
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})