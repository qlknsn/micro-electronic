// pages/index/index.js
import {
  listByTypeGg,
  getAssemblyAll,
  listByTypeshb,
  redidentAppeal,
  jscode2session,
  loginByOpenid
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    marquee: 0, //每次移动X坐标
    windowWidth: 0, //小程序宽度
    maxScroll: 0, //文本移动至最左侧宽度及文本宽度
    modelList: [{
        icon: 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/micro-electronic/%E5%AE%B6%E9%97%A8%E5%8F%A3.png',
        functionName: '家门口服务',
        functionUrl: ''
      },
      {
        icon: 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/micro-electronic/%E6%9D%A5%E6%B2%AA.png',
        functionName: '来/返沪人员',
        functionUrl: ''
      },
      {
        icon: 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/micro-electronic/%E5%A4%B1%E7%89%A9%E6%8B%9B%E9%A2%86.png',
        functionName: '失物招领',
        functionUrl: ''
      },
      {
        icon: 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/micro-electronic/%E7%89%A9%E4%B8%9A.png',
        functionName: '物业报修',
        functionUrl: ''
      },
      {
        icon: 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/micro-electronic/%E5%A5%BD%E4%BA%BA.png',
        functionName: '好人好事',
        functionUrl: ''
      },
    ],
    districtName: '',
    noticeContent: '',
    agendaList: [],
    reportList: [],
    searchtext: '',
    showDialog: false,
    sealStatus: false,
    showtalkDialog: false,
    talkParams: {
      appealReason: '',
      userUrn: ''
    },
    autoUnsealTime: '',
    interval: null
  },
  getappealReason(e) {
    this.data.talkParams.appealReason = e.detail.value
  },
  // 申诉
  redidentAppeal() {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.talkParams.userUrn = userInfo.userUrn
    redidentAppeal(this.data.talkParams).then(res => {
      if (res.data.result) {
        this.setData({
          showtalkDialog: false
        })
      }
    })
  },
  showTalkDialog() {
    this.setData({
      showtalkDialog: true
    })
    this.setData({
      showDialog: false
    })
  },
  hidetalkDialog() {
    this.setData({
      showtalkDialog: false
    })
  },
  showDialog() {
    this.setData({
      showDialog: true
    })
  },
  hideDialog() {
    this.setData({
      showDialog: false
    })
  },
  getsearch(e) {
    this.data.searchtext = e.detail.value
  },
  gotoSearch() {
    wx.navigateTo({
      url: '/pages/searchResult/searchResult?text=' + this.data.searchtext,
    })
  },
  gotoAgendalist() {

    let residentMenu = wx.getStorageSync('residentMenu')
    if (residentMenu) {
      wx.navigateTo({
        url: '/pages/agendaList/agendaList',
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '用户登录后才可在居委内使用此功能',
        success(res) {
          if (res.confirm) {
            wx.reLaunch({
              url: '/pages/register/register',
            })
          } else if (res.cancel) {}
        }
      })
    }
  },
  gotoReportlist() {
    wx.navigateTo({
      url: '/pages/reportList/reportList',
    })
  },
  scrolltxt: function () {
    let t = this;
    let d = t.data;

    d.interval = setInterval(function () {
      // console.log(d.marquee - 3)
      var next = d.marquee - 2; //每次移动距离
      if (next < 0 && Math.abs(next) > d.maxScroll) {
        next = d.windowWidth;
        clearInterval(d.interval);
        t.scrolltxt();
      }
      t.setData({
        marquee: next
      });
    }, 100);
  },
  gotoNotice() {
    let residentMenu = wx.getStorageSync('residentMenu')
    if (residentMenu) {
      wx.navigateTo({
        url: '/pages/noticeList/noticeList',
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '用户登录后才可在居委内使用此功能',
        success(res) {
          if (res.confirm) {
            wx.reLaunch({
              url: '/pages/register/register',
            })
          } else if (res.cancel) {}
        }
      })
    }
  },
  getAssemblyAlls() {
    let userInfo = wx.getStorageSync('userInfo')
    let params = {
      start: 0,
      count: 2,
      districtIds: userInfo.districtId,
      userUrn: userInfo.userUrn
      // mobile:userInfo.mobile,
    }
    getAssemblyAll(params).then(res => {
      this.setData({
        agendaList: res.data.results
      })
    })
  },
  listByTypeshbs() {
    let userInfo = wx.getStorageSync('userInfo')
    let params = {
      start: 0,
      count: 1,
      districtIds: userInfo.districtId,
      // mobile:userInfo.mobile,
    }
    listByTypeshb(params).then(res => {
      this.setData({
        reportList: res.data.results
      })
    })
  },
  addAgenda(){
    wx.navigateTo({
      url: '/pages/sendTrend/sendTrend',
    })
  },
  getNotice() {
    this.data.noticeContent = ''
    let params = {
      start: 0,
      count: 3,
      type: 3
    }
    listByTypeGg(params).then(res => {
      let results = res.data.results
      results.forEach((item, index) => {
        this.setData({
          noticeContent: this.data.noticeContent + (index + 1) + '.' + item.content + "   "
        })

        let t = this;
        var w = wx.getSystemInfoSync().windowWidth - 80;
        var str = this.data.noticeContent + (index + 1) + '.' + item.content + "   ";
        t.setData({
          marquee: w
        });
        t.data.maxScroll = str.length * 6;
        t.data.windowWidth = w;
        clearInterval(this.data.interval);
        t.scrolltxt();
      })
    })
  },
  gotoModel(e) {
    let residentMenu = wx.getStorageSync('residentMenu')
    if (residentMenu) {
      wx.navigateTo({
        url: e.currentTarget.dataset.url,
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '用户登录后才可在居委内使用此功能',
        success(res) {
          if (res.confirm) {
            wx.reLaunch({
              url: '/pages/register/register',
            })
          } else if (res.cancel) {}
        }
      })
    }

  },
  gotoHelp() {
    wx.navigateTo({
      url: '/pages/help/help',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // let t = this;
    // var w = wx.getSystemInfoSync().windowWidth - 80;
    // var str = '滚动文本信息';
    // t.setData({
    //   marquee: w
    // });
    // t.data.maxScroll = str.length * 64;
    // t.data.windowWidth = w;
    // t.scrolltxt();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let userStatus = wx.getStorageSync('userInfo').userStatus
    if (userStatus == 1) {
      this.getTabBar().setData({
        show: true
      })
    }
    if (userStatus == 0) {
      this.getTabBar().setData({
        show: false
      })
    }

  },
  getopenId(code) {
    return new Promise((resolve, reject) => {
      jscode2session({
        jsCode: code
      }).then(res => {
        wx.setStorageSync('sessionKey', res.data.result.sessionKey)
        resolve(res.data.result.openid)
      })
    })
  },
  login(openid) {
    loginByOpenid({
      openid: openid
    }).then(res => {
      // 判断是否有menu=》是否注册
      if (res.data.result.residentMenu) {
        wx.setStorageSync('userInfo', res.data.result.residentObject)
        wx.setStorageSync('residentMenu', res.data.result.residentMenu)
        wx.setStorageSync('staffObject', res.data.result.staffObject)
        wx.setStorageSync('token', res.data.result.token)
        if (res.data.result.staffObject) {
          wx.setStorageSync('canWuye', true)
        } else {
          wx.setStorageSync('canWuye', false)
        }
        // 获取当前小程序的页面栈
        let pages = getCurrentPages();
        // 数组中索引最大的页面--当前页面
        let currentPage = pages[pages.length - 1];
        // 打印出当前页面中的 options
        let options = currentPage.options
        let userStatus = wx.getStorageSync('userInfo').userStatus
        if (wx.getStorageSync('userInfo')) {
          this.getTabBar().setData({

            list: [{
                "pagePath": "/pages/index/index",
                "iconPath": "../image/home.png",
                "selectedIconPath": "../image/shouye@2x.png",
                "text": "首页"
              },
              {
                "pagePath": "/pages/help/help",
                "iconPath": "../image/qiuzhu@2x.png",
                "selectedIconPath": "../image/qiuzhu@2x.png",
                "text": "一键求助"
              },
              {
                "pagePath": "/pages/my/my",
                "iconPath": "../image/wode1@2x.png",
                "selectedIconPath": "../image/wode@2x.png",
                "text": "我的"
              }
            ],
          });
          this.getTabBar().setData({
            selected: 0,
          })
        }
        let residentMenu = wx.getStorageSync('residentMenu')
        let districtName = wx.getStorageSync('userInfo').districtName
        let autoUnsealTime = wx.getStorageSync('userInfo').autoUnsealTime
        this.setData({
          autoUnsealTime: autoUnsealTime
        })
        if (residentMenu) {
          this.setData({
            modelList: residentMenu
          })
        }
        this.setData({
          districtName: districtName
        })
        if (options.status == 'feng') {
          this.showDialog()
        } else {
          this.getAssemblyAlls()
          this.getNotice()
          this.listByTypeshbs()
        }
        if (userStatus && userStatus == 1) {
          this.setData({
            sealStatus: true
          })
          // wx.hideTabBar({
          //   animation: true,
          // })

        } else {
          this.setData({
            sealStatus: false
          })
          // wx.showTabBar({
          //   animation: true,
          // })
        }
        // 
      } else {
        wx.clearStorage()
        // wx.reLaunch({
        //   url: '/pages/index/index',
        // })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (wx.getStorageSync('peopleType') && wx.getStorageSync('peopleType') == 'wuye') {
      wx.switchTab({
        url: '/pages/historyFinish/historyFinish',
      })
    } else {
      wx.setStorageSync('peopleType', 'person')
    }
    let that = this
    wx.login({
      success: (res) => {
        that.getopenId(res.code).then(openid => {
          wx.setStorageSync('openId', openid)
          that.login(openid)
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})