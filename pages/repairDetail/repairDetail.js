// pages/repairDetail/repairDetail.js
import { getRepairDetail } from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    repairDetail: {},
    fileArr1: [],
    fileArr2: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    getRepairDetail({uniqueId: options.id}).then(res => {
      this.setData({
        repairDetail: res.data.result.guaranteeObject,
        fileArr1: res.data.result.filePaths,
        fileArr2: res.data.result.filePathSs
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})