// pages/noticeDetail/noticeDetail.js
import {getHomepage} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noticeDetail:{},
    IMG_HOST:'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/'
  },
  /**
   * 
   */
  voteOnClick:function(){
    console.log(this.data.noticeDetail);
    wx.navigateTo({
      url: '/pages/vote/vote?id='+this.data.noticeDetail.homepageThrObject.id
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    getHomepage({id:options.id,type:options.type}).then(res=>{
      this.setData({
        noticeDetail:res.data.result
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})