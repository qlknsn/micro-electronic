// pages/help/help.js
import {
  getHelpAll
} from '../../request/getIndexData'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      count: 10,
      start: 0
    },
    upcount: 0,
    helpList: []
  },
  callPhone(e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.item.mobile //仅为示例，并非真实的电话号码
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    // this.getTabBar().setData({
    //   selected: 1,
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.setData({
      params: {
        count: 10,
        start: 0
      },
    })
    getHelpAll(this.data.params).then(res => {
      this.setData({
        helpList: res.data.results
      })
    })
    this.getTabBar().setData({
      list: [{
          "pagePath": "/pages/index/index",
          "iconPath": "../image/home.png",
          "selectedIconPath": "../image/shouye@2x.png",
          "text": "首页"
        },
        {
          "pagePath": "/pages/help/help",
          "iconPath": "../image/qiuzhu@2x.png",
          "selectedIconPath": "../image/qiuzhu@2x.png",
          "text": "一键求助"
        },
        {
          "pagePath": "/pages/my/my",
          "iconPath": "../image/wode1@2x.png",
          "selectedIconPath": "../image/wode@2x.png",
          "text": "我的"
        }
      ],
    });
    this.getTabBar().setData({
      selected: 1
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.upcount++
    this.data.params.start = this.data.upcount * 10
    getHelpAll(this.data.params).then(res => {
      this.data.helpList = [...this.data.helpList, ...res.data.results]
      this.setData({
        helpList: this.data.helpList
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})