import request from './request'
export function getArea() {
  return request({
    url: '/public/api/hip/v1/district/getDistrictTree',
  })
}
export function doVote(data) { //投票
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/voteTable/doVote',
    data: data,
    header: header,
    method: 'post'
  })
}
export function voteDetail(data) { //投票详情
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/voteTable/myVote',
    data: data,
    header: header,
    method: 'get'
  })
}
export function getMyVoteList(data) { //我的投票列表
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/voteTable/list',
    data: data,
    header: header,

  })
}
export function getsomeDatas(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/dictionary/list',
    data: data,
    header: header
  })
}
export function personregister(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/redident/register',
    method: 'post',
    data: data,
    header: header
  })
}
export function getCommentList(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/comment/page',
    method: 'post',
    data: data,
    header: header
  })
}
export function getRepairList(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/listByType',
    data: data,
    header: header
  })
}
export function deleteRepair(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/deleteGuaranteeObject',
    data: data,
    header: header
  })
}
export function getRepairDetail(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/findGuaranteeByUniqueId',
    data: data,
    header: header
  })
}
export function getReportList(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/handle/listByType',
    data: data,
    header: header
  })
}
export function deleteReport(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/handle/deleteHandleObject',
    data: data,
    header: header
  })
}
export function personReport(data) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return request({
    url: '/public/api/hip/v1/shanghaiPerson/saveShanghaiPerson',
    data: data,
    method: 'post',
    header: header
  })
}


export function jscode2session(data) {
  return request({
    url: '/public/api/hip/v1/account/jscode2session',
    data: data
  })
}
export function loginByOpenid(data) {
  return request({
    url: '/public/api/hip/v1/account/loginByOpenid',
    data: data
  })
}
// 获取公告
export function listByTypeGg(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/homePage/listByType',
    data: data,
    header: header
  })
}
export function getAssemblyAll(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/account/assembly/getAssemblyAll',
    data: data,
    header: header
  })
}
// 查询随手报
export function listByTypeshb(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/handle/listByType',
    data: data,
    header: header
  })
}

// 点赞
export function dianzan(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/likes/like',
    data: data,
    method: 'post',
    header: header
  })
}
// 取消点赞dis
export function canceldianzan(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/likes/cancel',
    data: data,
    header: header
  })
}
// 评论
export function makeComment(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/comment/makeComment',
    data: data,
    method: 'post',
    header: header
  })
}
// 增加议事厅
export function saveAssembly(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/account/assembly/saveAssembly',
    data: data,
    method: 'post',
    header: header
  })
}
// 议事厅详情
export function findHandleByUniqueId(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/account/assembly/findAssemblyByUniqueId',
    data: data,
    header: header
  })
}
// 随手报详情
export function findAssemblyByUniqueId(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/handle/findHandleByUniqueId',
    data: data,
    header: header
  })
}
// 增加随手报
export function saveHandle(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/handle/saveHandle',
    data: data,
    method: 'post',
    header: header
  })
}
// 查询字典
export function dictionaryList(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/dictionary/list',
    data: data,
    header: header
  })
}
// 查询公告、好人好事、失物招领
export function listByType(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/homePage/listByType',
    data: data,
    header: header
  })
}
// 查询公告、好人好事、失物招领详情
export function getHomepage(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/homePage/getHomepage',
    data: data,
    header: header
  })
}
// 物业报修添加
export function saveGuarantee(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/saveGuarantee',
    data: data,
    method: 'post',
    header: header
  })
}
// 一键求助
export function getHelpAll(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/help/getHelpAll',
    data: data,
    header: header
  })
}
// 一键求助
export function homeService(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/homeService/page',
    data: data,
    method: 'post',
    header: header
  })
}
// 评论列表
export function findTree(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/comment/findTree',
    data: data,
    header: header
  })
}
// 删除评论
export function deleteAssembly(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/account/assembly/deleteAssembly',
    data: data,
    header: header
  })
}
// 家门口服务列表查询
export function listByProject(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/homeService/listByProject',
    data: data,
    method: 'post',
    header: header
  })
}
// 家门口服务详情
export function homeServiceInfo(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/homeService/info',
    data: data,
    header: header
  })
}
// 预约
export function saveServiceTime(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/booking/save',
    data: data,
    method: 'post',
    header: header
  })
}
// 取消预约
export function deleteByServiceUrnAndAndUserUrn(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/booking/deleteByServiceUrnAndAndUserUrn',
    data: data,
    header: header
  })
}
// 申诉
export function redidentAppeal(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/redident/appeal',
    method: 'post',
    data: data,
    header: header
  })
}
// 报修列表
export function getGuaranteeH(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/getGuaranteeH',
    method: 'post',
    data: data,
    header: header
  })
}
// 报修处置
export function handleGuarantee(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/handleGuarantee',
    method: 'post',
    data: data,
    header: header
  })
}
// 报修详情
export function findGuaranteeByUniqueIde(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token: token
  }
  return request({
    url: '/public/api/hip/v1/guarantee/findGuaranteeByUniqueId',
    method: 'get',
    data: data,
    header: header
  })
}

// 我的预约
export function getMyOrder(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token
  }
  return request({
    url: '/public/api/hip/v1/booking/list',
    method: 'get',
    data,
    header
  })
}

// 取消预约
export function cancelOrder(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token
  }
  return request({
    url: '/public/api/hip/v1/booking/deleteByServiceUrnAndAndUserUrn',
    method: 'get',
    data,
    header
  })
}

// 获取手机号
export function getPhoneNumber(data) {
  let token = wx.getStorageSync('token')
  let header = {
    token
  }
  return request({
    url: '/public/api/hip/v1/account/phone',
    method: 'post',
    data,
    header
  })
}