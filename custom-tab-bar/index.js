const app = getApp();
Component({
  data: {
    selected: 0,
    color: "#7A7E83",
    selectedColor: "#0389E3",
    list: [],
    show: false,
  },
  lifetimes: {
    //组件的生命周期函数
    attached(e) {
    },
  },
  ready() {
    // this.setData({
    //   selected: 0,
    // });
  },

  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset;
      const url = data.path;
      wx.switchTab({
        url,
      });
      this.setData({
        selected: data.index,
      });
    },
  },
});