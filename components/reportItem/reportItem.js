// components/reportItem/reportItem.js
import {
  dianzan,
  canceldianzan
} from '../../request/getIndexData'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item:{
      type:Object,
      value:{}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    data_item:{},
    showDisducss:false,
    IMG_HOST:'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getReportItem(e){
      wx.navigateTo({
        url: '/pages/reportDetail/reportDetail?uniqueId='+e.currentTarget.dataset.item.uniqueId,
      })
    },
    hideDiscuss(e){
      this.setData({
        showDisducss:e.detail
      })
    },
    showDiscuss(){
      this.setData({
        showDisducss:true
      })
    },
    dianzans(e) {
      let item = this.data.data_item
      let userInfo = wx.getStorageSync('userInfo')
      if (item.isLike) {
        // 已经点过赞将取消点赞
        let params = {
          refUrn: this.data.data_item.uniqueId,
          urn: userInfo.userUrn,
        }
        canceldianzan(params).then(res=>{
          if(res.data.error){
            wx.showToast({
              title: '取消点赞失败',
            })
          }else{
            this.data.data_item.isLike = false
            this.data.data_item.likesCount--
            this.setData({
              data_item: this.data.data_item
            })
          }
        })
      } else {
        let params = {
          refUrn: this.data.data_item.uniqueId,
          urn: userInfo.userUrn,
          name: userInfo.nickname
        }
        // 没有点过赞
        dianzan(params).then(res => {
          if (res.data.error) {
            wx.showToast({
              title: '点赞失败',
            })
          } else {
            this.data.data_item.isLike = true
            this.data.data_item.likesCount++
            this.setData({
              data_item: this.data.data_item
            })
          }
        })
      }
    }
  },
  attached: function () {
    // 在组件实例进入页面节点树时执行]
    this.setData({
      data_item: this.properties.item
    })
  },
  observers:{
    'item':function(data){
      if(data.id){
        this.setData({
          data_item: data
        })
      }
    }
  }
})
