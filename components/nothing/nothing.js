// pages/my/my.js
Component({
  properties: {
    propA: String
  },
  /**
   * 页面的初始数据
   */
  data: {
    showChanfePanel: false
  },
  methods:{
    toReport: function() {
      wx.navigateTo({
        url: '/pages/addReport/addReport',
      })
    },
    toRepair: function() {
      wx.navigateTo({
        url: '/pages/wuyeRepair/wuyeRepair',
      })
    },
    toActive: function() {
      wx.navigateTo({
        url: '/pages/sendTrend/sendTrend',
      })
    }
  }
})