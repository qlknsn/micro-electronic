// components/discussItem/discussItem.js
import {
  makeComment
} from '../../request/getIndexData'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {}
    },
    currentDis: {
      type: Object,
      value: {}
    },
    type: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    callName: '',
    inputVal: '',
    focusInput: true,
    isInput: true,
    reasonHeight:0,
    params: {
      commentUserName: '',
      commentUserUrn: null,
      objectPortraitImg: '',
      content: '',
      pcommentId: null,
      pid: null,
      portraitImg: '',
      refUrn: '',
      userName: '',
      userUrn: ''
    }
  },
  attached() {
    let userInfo = wx.getStorageSync('userInfo')
    this.data.params.userName = userInfo.nickname
    this.data.params.userUrn = userInfo.userUrn
    this.data.params.portraitImg = userInfo.portraitImg
    let uni = this.properties.item.uniqueId

    if (this.properties.item.id) {
      this.data.params.refUrn = uni
      this.setData({
        callName: this.properties.item.name
      })
    }
    if (this.properties.currentDis.id) {
      this.data.params.refUrn = this.properties.currentDis.refUrn
      this.setData({
        callName: this.properties.currentDis.userName
      })
      this.data.params.commentUserName = this.properties.currentDis.userName
      this.data.params.commentUserUrn = this.properties.currentDis.userUrn
      if (this.properties.type == 'first') {
        this.data.params.pid = this.properties.currentDis.id
      } else if (this.properties.type == 'origin') {
        this.data.params.pcommentId = null
        this.data.params.pid = null

      } else {
        this.data.params.pid = this.properties.currentDis.pid
        this.data.params.pcommentId = this.properties.currentDis.id
      }

      this.data.params.objectPortraitImg = this.properties.currentDis.portraitImg
    }

  },
  /**
   * 组件的方法列表
   */
  methods: {
    setPoi(e) {
      this.setData({
        isInput: true
      })
      let vm = this;
      vm.setData({
        reasonHeight: e.detail.height || 0
      })

    },
    bindblurDialog() {
      let vm = this;
      vm.setData({
        reasonHeight: 0
      })
      this.triggerEvent('myevent', false)
    },
    inputBlur() {
      this.triggerEvent('myevent', false)
    },
    getValue(e) {
      let val = e.detail.value.trim()
      this.data.params.content = val
      this.setData({
        inputVal: val
      })
    },
    hideDiscuss() {

      // 发布评论。
      if (this.data.inputVal.length > 0) {

        makeComment(this.data.params).then(res => {
          if (res.data.result) {
            wx.showToast({
              title: '评论成功，需要系统审核',
              icon: 'none',
              duration: 2000
            })
            setTimeout(() => {
              this.triggerEvent('myevent', false)
            }, 2000)
          } else {
            wx.showToast({
              title: res.data.error.message,
              icon: 'none',
              duration: 2000
            })
            setTimeout(() => {
              this.triggerEvent('myevent', false)
            }, 2000)
          }
        })
      } else {
        this.triggerEvent('myevent', false)
      }

    }
  }
})