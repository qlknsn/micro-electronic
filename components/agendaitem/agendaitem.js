// components/agendaitem/agendaitem.js
import {
  dianzan,
  canceldianzan,
  deleteAssembly
} from '../../request/getIndexData'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {}
    },
    showMine:{
      type:Boolean
    },
    index:{
      type:Number
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    data_item: 1,
    showDisducss:false,
    IMG_HOST:'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/',
    showzanImg:false
  },
  
  /**
   * 组件的方法列表
   */
  attached: function () {
    // 在组件实例进入页面节点树时执行]

    this.setData({
      data_item: this.properties.item
    })
  },
  methods: {
    getAgendaItem(e){
      wx.navigateTo({
        url: '/pages/agendaDetail/agendaDetail?uniqueId='+e.currentTarget.dataset.item.uniqueId,
      })
    },
    hideDiscuss(e){
      this.setData({
        showDisducss:e.detail
      })
    },
    showDiscuss(){
      this.setData({
        showDisducss:true
      })
    },
    dianzans(e) {
      let item = this.data.data_item
      let userInfo = wx.getStorageSync('userInfo')
      if (item.isLike) {
        // 已经点过赞将取消点赞
        let params = {
          refUrn: this.data.data_item.uniqueId,
          urn: userInfo.userUrn,
        }
        canceldianzan(params).then(res=>{
          if(res.data.error){
            wx.showToast({
              title: '取消点赞失败',
            })
          }else{
            this.data.data_item.isLike = false
            this.data.data_item.likesCount--
            this.setData({
              data_item: this.data.data_item
            })
          }
        })
      } else {
        let params = {
          refUrn: this.data.data_item.uniqueId,
          urn: userInfo.userUrn,
          name: userInfo.nickname
        }
        // 没有点过赞
        dianzan(params).then(res => {
          if (res.data.result) {
            
            this.data.data_item.isLike = true
            this.data.data_item.likesCount++
            this.setData({
              data_item: this.data.data_item
            })
            this.setData({
              showzanImg:true
            })
            setTimeout(()=>{
              this.setData({
                showzanImg:false
              })
            },1000)
          
          }else {
            wx.showToast({
              title: '你已经赞过了哦',
              icon:'none'
            })
          }
        })
      }
    },
    deleteAgenda(e){
      let dataid = e.currentTarget.dataset.item.uniqueId
      let that = this
      wx.showModal({
        title: '提示',
        content: '删除后本条动态将从列表消失~',
        success (res) {
          if (res.confirm) {
            deleteAssembly({uniqueId:dataid}).then(res=>{
              if(res.data.result){
                that.triggerEvent('delete',that.properties.index)
              }
            })
            
          } else if (res.cancel) {
          }
        }
      })
      
    },
  }
})