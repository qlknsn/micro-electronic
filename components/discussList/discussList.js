// components/discussList/discussList.js
import {
  dianzan,
  canceldianzan,
} from '../../request/getIndexData'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    discusslist: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    showDisducss: false,
    IMG_HOST: 'https://bh-zhoupo.oss-cn-shanghai.aliyuncs.com/',
    discussType: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    dianzans(e) {
      let userInfo = wx.getStorageSync('userInfo')
      let data = e.currentTarget.dataset.item.comment
      let index = e.currentTarget.dataset.index
      if (data.isLike) {
        let params = {
          refUrn: data.id,
          urn: userInfo.userUrn,
        }
        canceldianzan(params).then(res => {
          if (res.data.result) {

            this.data.discusslist[index].comment.isLike = false
            this.data.discusslist[index].likesCount--
            this.setData({
              discusslist: this.data.discusslist
            })
          }
        })
      } else {
        let params = {
          refUrn: data.id,
          urn: userInfo.userUrn,
          name: userInfo.nickname
        }
        dianzan(params).then(res => {
          if (res.data.result) {

            this.data.discusslist[index].comment.isLike = true
            this.data.discusslist[index].likesCount++
            this.setData({
              discusslist: this.data.discusslist
            })
          }
        })
      }
    },
    hideDiscuss(e) {
      this.setData({
        showDisducss: e.detail
      })
    },
    showDiscuss(e) {
      console.log(e.currentTarget.dataset)
      this.setData({
        discussType: e.currentTarget.dataset.type
      })

      this.setData({
        currentdiscuss: e.currentTarget.dataset.current.comment
      })
      this.setData({
        showDisducss: true
      })
    },
    showsecondDiscuss(e) {
      this.setData({
        secondcurrentdiscuss: e.currentTarget.dataset.current.comment
      })
      this.setData({
        showDisducss: true
      })
    },
  }
})